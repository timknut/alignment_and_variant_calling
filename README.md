# Aligment and variant calling.
Code examples and information on aligment and variants calling for 
Cigene/Geno/Aquagen. 
## Structural Variant (SV) Calling.
Detect SVs from short read sequence data. Illumina data. Best practise is to 
merge results from several callers, or do trio-based SV-calling.
Or both mentioned. See https://github.com/zeeev/wham#wham 
## Cool links
[Ben on CNVs](https://gsejournal.biomedcentral.com/articles/10.1186/s12711-017-0286-5)
Ben Hayes et al. use agressive filtering and famility trios to filter. 

Brad Chapman is a champion: https://bcbio.wordpress.com/2014/08/12/validated-whole-genome-structural-variation-detection-using-multiple-callers/ 
### Breakpoint based
[Lumpy](https://github.com/arq5x/lumpy-sv)

[dellyexpress](https://github.com/dellytools/delly)

[whamg](https://github.com/zeeev/wham)
### Coverage based
[CNV-seq](http://tiger.dbs.nus.edu.sg/cnv-seq/) Used in the mastitis paper. 
Good when you have groups to compare, 
e.g. based on oposite homozygous QTL-genotype.

[CNVkit](http://cnvkit.readthedocs.io/en/latest/index.html)
### Ensembl calling
[metaSV](http://bioinform.github.io/metasv/)

### SV toolkit. 
[svtools](https://github.com/hall-lab/svtools) can merge SV calls from thousands 
of sampels, compute SV genotypes based on break point coverage, make BED files of 
SV-calls for IGV viewing etc.
## Aligmnet
[speedseq](https://github.com/hall-lab/speedseq)

### Read groups
Info på read groups: http://gatkforums.broadinstitute.org/gatk/discussion/6472/read-groups

```sh
tikn@login-0:~/SeqData3/speedseq_SV$ cat speedseq_realign.sh 
#!/bin/bash

#SBATCH -N 1
#SBATCH -n 5
#SBATCH --partition=cigene,hugemem,verysmallmem
#SBATCH --output=slurm_logs/bam_%A_%a.log  ##  when array job  

module load speedseq

set -o nounset   # Prevent unset variables from been used.
set -o errexit   # Exit if error occurs

## Usage: sbatch -a 1-29 conform_25K_50K.sh arrayfile

arrayfile=$1
TASK=$SLURM_ARRAY_TASK_ID
bam=$(awk ' NR=='$TASK' { print $1 ; }' $arrayfile)
bam_out=bams/$(basename $bam .bam).realign
ref=~/seqdata1/bovine/reference/1000bulls/umd_3_1_reference_1000_bull_genomes.no_unplaced.fa

speedseq realign -t 5 -T /work/users/tikn/${RANDOM}_${RANDOM} -M 20 -o $bam_out $ref $bam
```

`sbatch -a 1-10 speedseq_realign.sh array_file.txt`

```sh
head array_file.txt 
/mnt/users/tikn/SeqData3/fasteris_aros_realign_2015/5277/5277_bwakit_realign.bam
/mnt/users/tikn/SeqData3/fasteris_aros_realign_2015/5583/5583_bwakit_realign.bam
/mnt/users/tikn/SeqData3/fasteris_aros_realign_2015/5694/5694_bwakit_realign.bam
/mnt/users/tikn/SeqData3/fasteris_aros_realign_2015/10540/10540_bwakit_realign.bam
/mnt/users/tikn/SeqData3/fasteris_aros_realign_2015/10177/10177_bwakit_realign.bam
```

**Gives**
```
5277_bwakit_realign.realign.bam
5277_bwakit_realign.realign.bam.bai
5277_bwakit_realign.realign.discordants.bam
5277_bwakit_realign.realign.discordants.bam.bai
5277_bwakit_realign.realign.splitters.bam
5277_bwakit_realign.realign.splitters.bam.bai
```

Aligns, sorts, and indexes paired end reads with one command. (BWA-mem). 
Also extracts discordant and split reads for use with the SV-calling module. Runs LUMPY. 



[bwa-kit](https://github.com/lh3/bwa/tree/master/bwakit)

```sh
# Download the bwa-0.7.11 binary package (download link may change)
wget -O- http://sourceforge.net/projects/bio-bwa/files/bwakit/bwakit-0.7.12_x64-linux.tar.bz2/download \
  | gzip -dc | tar xf -
# Generate the GRCh38+ALT+decoy+HLA and create the BWA index
bwa.kit/run-gen-ref hs38DH   # download GRCh38 and write hs38DH.fa
bwa.kit/bwa index hs38DH.fa  # create BWA index
# mapping
bwa.kit/run-bwamem -o out -H hs38DH.fa read1.fq read2.fq | sh
```
Does mapping, sorting and indexing with one command. Does not output split and discordant reads. 

## Variant calling
[GATK](https://software.broadinstitute.org/gatk/best-practices/bp_3step.php?case=GermShortWGS)

[Freebayes](https://github.com/ekg/freebayes)

```sh
tikn@login-0:~/SeqData3/variant_calling$ cat array_freebayes.sh 
#!/bin/sh
#SBATCH -J freebayes-array
#SBATCH -n 1
#SBATCH --partition=cigene,hugemem
#SBATCH -N 1
#SBATCH --output=slurm_logs/fbFA_array_%A_region%a.log

## run with sbatch -a 1-100 array_freebayes.sh bamlist.txt regionlist.txt

#module load freebayes/1.0.2 # use conda version in $PATH Used for all other Chroms than 5 and 6
module load freebayes/1.1.0
module load samtools ## newest version 1.3
prefix=FA_calling_fullChrs
#mkdir -p $prefix
#freebayes=~/bioinf_tools/freebayes/bin/freebayes

TASK=$SLURM_ARRAY_TASK_ID
ref=~/seqdata1/bovine/reference/1000bulls/umd_3_1_reference_1000_bull_genomes.fa
bamlist=$1
regions=$2
# targets=coverage_2_bed/gc_area_highcov_to_use.bed Does not work with array script setting up 
# the --region arg. 

region=$( awk ' NR=='$TASK' { print $1 ; }' $regions)
name=$(echo $region | sed 's/:/_/')

if [ ! -d "$prefix" ]; then
	mkdir -p "$prefix"
fi

# freebayes with moderate filtering.
echo "running region $region"
echo
echo "running command:"
echo
echo \
"freebayes --region $region \
   -f $ref \
   --bam-list $bamlist \
   --use-mapping-quality \
   --min-alternate-count 2 \
   --genotype-qualities \
   --min-alternate-fraction 0.2 \
   --min-coverage 10 \
   --no-mnps \
   --no-complex \
   -m 20 -q 20 \
   | vcffilter -f 'QUAL > 30' \
   | bgzip  > ${prefix}/${name}_QUAL_30.vcf.gz \
	&& tabix ${prefix}/${name}_QUAL_30.vcf.gz"

time freebayes --region $region \
   -f $ref \
   --bam-list $bamlist \
   --use-mapping-quality \
   --min-alternate-count 2 \
   --genotype-qualities \
   --min-alternate-fraction 0.2 \
   --min-coverage 10 \
   --no-mnps \
   --no-complex \
   -m 20 -q 20 \
   | vcffilter -f 'QUAL > 30' \
   | bgzip  > ${prefix}/${name}_QUAL_30.vcf.gz \
	&& tabix ${prefix}/${name}_QUAL_30.vcf.gz
#
# With stringent MQ and baseQual filters. From freebayes manual.
#   -m 30 -q 20 \
## --max-coverage cause error at the moment.

  # --max-coverage 50000 \
```


### Filtering

```sh
tikn@login-0:~/SeqData3/variant_calling$ cat vcf_filt.sh 
#!/bin/bash
#SBATCH -N 1
#SBATCH -n 10
#SBATCH --partition=cigene,hugemem

set -o nounset   # Prevent unset variables from been used.
set -o errexit   # Exit if error occurs

vcf_in=$1
vcf_out=$(dirname $vcf_in)/$(basename ${vcf_in} .vcf.gz).filt.vcf.gz
nt=$SLURM_CPUS_ON_NODE

## Below filters are taken directly from the 1kbulls filters run 5.
## Reduced --SnpGap from 5 to 4, since freebayes is good at local realignment. 
#   -e "NUMALT >= 2 | SAF == 0 | SAR == 0 | DP <= 10 | DP >= 2000" \
#   --SnpGap 4 \
#   --IndelGap 10 \

## After bcb.io recommendatins:
# bcftools filter -e "NUMALT > 1 && SAF <= 1 && SAR <= 1 && DP <= 10 && (QUAL < 5000 && DP > 2500)" Chr6_92800000-92900000_QUAL_30.vcf.gz

## Include 5.4.2016
## Test with filter
## Soft filter	--soft-filter 1kbulls "$vcf_in" | \

echo "filters: -e 'NUMALT >= 2 | SAF == 0 | SAR == 0 | DP < 10 | DP > 2500 | MAF < 0.01'"
echo "--SnpGap 4 --IndelGap 10"
echo
bcftools filter \
	--exclude "NUMALT >= 2 | SAF == 0 | SAR == 0 | DP < 10 | DP > 2500 | MAF < 0.01" \
	--SnpGap 4 \
	--IndelGap 10 \
	-o $vcf_out \
	-O z \
	--threads $nt \
	"$vcf_in" 

```


## The N+1 problem. gVCF osv..
GATK has soloution. Freebayes getting there.. 
GATK is really slow anyway. 

**Recommended**:
Freebayes on all BAMS, but remove high coverage areas with coverage analysis and some bedtools magic on BAMS. 
